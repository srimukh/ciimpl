# Pull base image
FROM openjdk:8-jre-alpine
FROM tomcat:8-jre8


# Copy to images tomcat path
ADD ./JMS-ActiveMQ-SenderApp/target/JMS*.war /usr/local/tomcat/webapps/JMSSender.war
RUN sed -i 's/port="8080"/port="8081"/' /usr/local/tomcat/conf/server.xml
CMD ["catalina.sh", "run"]
