package com.miracle.test.jmsactivemq.sender;

import java.net.InetSocketAddress;
import java.util.concurrent.TimeUnit;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.stereotype.Component;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import com.codahale.metrics.annotation.Metric;
import com.codahale.metrics.graphite.Graphite;
import com.codahale.metrics.graphite.GraphiteReporter;

/**
 * @author SuRyA
 *
 */
@Component
public class Producer{
	//private String url = "tcp://devopsapp.southcentralus.cloudapp.azure.com:61616";
	private String url = "tcp://queue:61616";
	private ConnectionFactory factory = null;
	private Connection connection = null;
	private Session session = null;
	private Destination destination = null;
	private MessageProducer producer = null;
	private TextMessage message = null;

	@Metric
	static final MetricRegistry metrics = new MetricRegistry();
	private static final String GRAPHITE_HOST = "http://192.168.1.230/";
	private static final int GRAPHITE_PORT = 8080;
	
	
	//This function takes the input from the user through UI and sends the message to queue
	public void sendMessage(String messageText) {
		
		//Timer.Context context = MetricsReporterConfig.metricRegistry().timer(MetricRegistry.name(Producer.class, "Sample")).time();
		Timer.Context time = null;
		try {
			Timer requests = metrics.timer("request-processed");
			time = requests.time();
			//Creating a factory object 
			 factory = new ActiveMQConnectionFactory("admin", "admin", url);
			//factory = new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_BROKER_URL);
			//Creating a Connection Object - Need to make connection to ActiveMQ
			 connection = factory.createConnection();
			connection.start();
			//Creating a session object
			 session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			//Creating a destination queue by name "SAMPLEQUEUE"
			 destination = session.createQueue("SAMPLEQUEUE");
			//Creating a producer object 
			 producer = session.createProducer(destination);
			//creating a message object
			 message = session.createTextMessage();
			message.setText(messageText);
			for(int i=0; i<10; i++)
			{
			producer.send(message);
			}
			Producer p  = new Producer();
			p.startReport1(metrics);
			System.out.println("Sent:" +messageText);
		} catch (JMSException e) {
			e.printStackTrace();
		}
		
		finally
		{
			time.stop();
		}	
	}	
	public void startReport1(MetricRegistry m) {
		Graphite graphite = (Graphite) new Graphite(new InetSocketAddress(  GRAPHITE_HOST, GRAPHITE_PORT));
	        
		 GraphiteReporter reporter =GraphiteReporter.forRegistry(m)
	                .convertRatesTo(TimeUnit.SECONDS)
	                .convertDurationsTo(TimeUnit.MILLISECONDS)
	                .build(graphite);
	        (reporter).start(1, TimeUnit.MINUTES);
	    }

}
