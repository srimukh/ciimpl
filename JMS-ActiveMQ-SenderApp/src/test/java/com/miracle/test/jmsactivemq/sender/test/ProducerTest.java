package com.miracle.test.jmsactivemq.sender.test;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.miracle.test.jmsactivemq.controller.Controller;
import com.miracle.test.jmsactivemq.sender.Producer;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Producer.class, ConnectionFactory.class, ActiveMQConnectionFactory.class})
public class ProducerTest {
	
	@Test
	public void test() throws Exception {
		String message = "Hello";
	
		Controller cont = new Controller();	
		Producer pmock = PowerMockito.mock(Producer.class);
		PowerMockito.whenNew(Producer.class).withNoArguments().thenReturn(pmock);
		Connection cmock = PowerMockito.mock(Connection.class);
		Session smock = PowerMockito.mock(Session.class);
		Queue qmock = PowerMockito.mock(Queue.class);
		TextMessage Tmock = PowerMockito.mock(TextMessage.class);
		MessageProducer mpmock = PowerMockito.mock(MessageProducer.class);
		ActiveMQConnectionFactory amconmock = PowerMockito.mock(ActiveMQConnectionFactory.class);
		PowerMockito.whenNew(ActiveMQConnectionFactory.class).withArguments("admin","admin" ,"tcp://devopsapp.southcentralus.cloudapp.azure.com:61616" ).thenReturn(amconmock);
		PowerMockito.when(amconmock.createConnection()).thenReturn(cmock);
		PowerMockito.when(cmock.createSession(any(Boolean.class), any(Integer.class))).thenReturn(smock);
		PowerMockito.when(smock.createQueue(any(String.class))).thenReturn(qmock);
		PowerMockito.when(smock.createProducer(qmock)).thenReturn(mpmock);
		PowerMockito.when(smock.createTextMessage()).thenReturn(Tmock);
		cont.test(message);
		assertTrue(true);
		
	}

	@Test
	public void testSendMessage() throws Exception {
		
		String message = "Hello";
		Producer prod = new Producer();
		Connection cmock = PowerMockito.mock(Connection.class);
		Session smock = PowerMockito.mock(Session.class);
		Queue qmock = PowerMockito.mock(Queue.class);
		TextMessage Tmock = PowerMockito.mock(TextMessage.class);
		MessageProducer mpmock = PowerMockito.mock(MessageProducer.class);
		ActiveMQConnectionFactory amconmock = PowerMockito.mock(ActiveMQConnectionFactory.class);
		PowerMockito.whenNew(ActiveMQConnectionFactory.class).withArguments("admin","admin" ,"tcp://devopsapp.southcentralus.cloudapp.azure.com:61616" ).thenReturn(amconmock);
		PowerMockito.when(amconmock.createConnection()).thenReturn(cmock);
		PowerMockito.when(cmock.createSession(any(Boolean.class), any(Integer.class))).thenReturn(smock);
		PowerMockito.when(smock.createQueue(any(String.class))).thenReturn(qmock);
		PowerMockito.when(smock.createProducer(qmock)).thenReturn(mpmock);
		PowerMockito.when(smock.createTextMessage()).thenReturn(Tmock);
		prod.sendMessage(message);
		
	}
	
	@Test
	public void negtestSendMessage() throws Exception {
		String message = "Hello";
	
		Producer prod = new Producer();
		Connection cmock = PowerMockito.mock(Connection.class);
		Session smock = PowerMockito.mock(Session.class);
		Queue qmock = PowerMockito.mock(Queue.class);
		MessageProducer mpmock = PowerMockito.mock(MessageProducer.class);
		ActiveMQConnectionFactory amconmock = PowerMockito.mock(ActiveMQConnectionFactory.class);
		PowerMockito.whenNew(ActiveMQConnectionFactory.class).withArguments("admin","admin" ,"tcp://devopsapp.southcentralus.cloudapp.azure.com:61616" ).thenReturn(amconmock);
		PowerMockito.when(amconmock.createConnection()).thenReturn(cmock);
		PowerMockito.when(cmock.createSession(any(Boolean.class), any(Integer.class))).thenReturn(smock);
		PowerMockito.when(smock.createQueue(any(String.class))).thenReturn(qmock);
		PowerMockito.when(smock.createProducer(qmock)).thenReturn(mpmock);
		PowerMockito.when(smock.createTextMessage()).thenThrow(new JMSException("dummy"));
		prod.sendMessage(message);

	}
	
}
